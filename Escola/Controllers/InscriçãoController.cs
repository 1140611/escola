﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Escola.Models;
using Escola.DAL;

namespace Escola.Controllers
{
    public class InscriçãoController : Controller
    {
        private EscolaContext db = new EscolaContext();

        // GET: /Inscrições/
        public ActionResult Index()
        {
            var inscrições = db.Inscrições.Include(i => i.UC).Include(i => i.Aluno);
            return View(inscrições.ToList());
        }

        // GET: /Inscrições/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inscrição inscrição = db.Inscrições.Find(id);
            if (inscrição == null)
            {
                return HttpNotFound();
            }
            return View(inscrição);
        }

        // GET: /Inscrições/Create
        public ActionResult Create()
        {
            PopulateDropDownListUCs();
            PopulateDropDownListAlunos();

            return View();
        }

         private void PopulateDropDownListUCs(object ucID = null)
         {
             var UCs = db.UCs.ToList().OrderBy(uc=>uc.Sigla);

             ViewBag.UCID = new SelectList(UCs, "ID", "Sigla", ucID);

             var i = 1;
         }

         private void PopulateDropDownListAlunos(object alunoID = null)
         {
             var Alunos = db.Alunos.ToList().OrderBy(a => a.Pronome);

             ViewBag.AlunoID = new SelectList(Alunos, "ID", "Pronome", alunoID);
             var i = 1;
         }

        // POST: /Inscrições/Create
		// To protect from over posting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		// 
		// Example: public ActionResult Update([Bind(Include="ExampleProperty1,ExampleProperty2")] Model model)
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Nota, DataDeNota, UCID, AlunoID, DataInscrição")]Inscrição inscrição)
        {
            if (ModelState.IsValid)
            {
                db.Inscrições.Add(inscrição);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            PopulateDropDownListUCs(inscrição.UCID);
            PopulateDropDownListAlunos(inscrição.AlunoID);

            return View(inscrição);
        }

        // GET: /Inscrições/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inscrição inscrição = db.Inscrições.Find(id);
            if (inscrição == null)
            {
                return HttpNotFound();
            }
            PopulateDropDownListUCs(inscrição.UCID);
            PopulateDropDownListAlunos(inscrição.AlunoID);

            return View(inscrição);
        }

        // POST: /Inscrições/Edit/5
		// To protect from over posting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		// 
		// Example: public ActionResult Update([Bind(Include="ExampleProperty1,ExampleProperty2")] Model model)
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID, Nota, DataDeNota, UCID, AlunoID, DataInscrição")]Inscrição inscrição)
        {
            if (ModelState.IsValid)
            {
                db.Entry(inscrição).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            PopulateDropDownListUCs(inscrição.UCID);
            PopulateDropDownListAlunos(inscrição.AlunoID);

            return View(inscrição);
        }

        // GET: /Inscrições/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inscrição inscrição = db.Inscrições.Find(id);
            if (inscrição == null)
            {
                return HttpNotFound();
            }
            return View(inscrição);
        }

        // POST: /Inscrições/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Inscrição inscrição = db.Inscrições.Find(id);
            db.Inscrições.Remove(inscrição);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
