﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Escola.Models
{
    public class Inscrição
    {
        public int ID { get; set; }
        public int UCID { get; set; }
        public virtual UC UC { get; set; }
        public int AlunoID { get; set; }
        public virtual Aluno Aluno { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Data de Inscrição")]
        public DateTime DataInscrição { get; set; }
        public int? Nota { get; set; }
        public DateTime? DataDeNota { get; set; }
    }
}