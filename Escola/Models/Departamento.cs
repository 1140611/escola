﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Escola.Models
{
    public class Departamento
    {
        public int ID { get; set; }

        public string Sigla { get; set; }
        public string Denominação { get; set; }
        public virtual ICollection<UC> UCs { get; set; }
    }
}