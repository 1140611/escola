﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Escola.Models
{
    public class UC
    {
        public int ID {get; set;}

        public string Sigla {get; set;}
        public int Créditos { get; set; }
        [Required(ErrorMessage = "Deve escolher um Departamento.")]
        public int DepartamentoID { get; set; }
        public virtual Departamento Departamento { get; set; }

        public virtual ICollection<Inscrição> Inscrições { get; set; }
    }
}