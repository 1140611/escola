﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Escola.Models
{
    public class AlunoDepartamento
    {
        public List<Aluno> alunos { get; set; }
        public List<Departamento> departamentos {get; set;}
    }
}